/**
 * https://developers.google.com/apps-script/guides/bound
 */
function onOpen()
{
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Artéfacts')
      .addItem('Chercher une feuille', 'search_sheet')
      .addSeparator()
      //.addSubMenu(ui.createMenu('Sub-menu')
      //    .addItem('Second item', 'menuItem2'))
      .addToUi();
}

/**
 * Authorization:
 *  scopes:
 *   https://www.googleapis.com/auth/script.container.ui
 *
 */
function search_sheet()
{
  var htmlOutput = HtmlService
    .createHtmlOutputFromFile('search_sheet_html')
    .setWidth(350)
    .setHeight(300);
  SpreadsheetApp.getUi().showModalDialog(htmlOutput, 'Search a sheet');
}

function getSheets()
{
  var spreadsheet = SpreadsheetApp.getActive();
  var sheets = spreadsheet.getSheets();
  var result = [];
  for( i = 0; i < sheets.length; i++ )
  {
    result.push( sheets[i].getName() );
  }
  return result ;
}

function activateSheet( sheet )
{
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.setActiveSheet( spreadsheet.getSheetByName(sheet) );
}
